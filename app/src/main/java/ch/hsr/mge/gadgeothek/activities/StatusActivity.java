package ch.hsr.mge.gadgeothek.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import ch.hsr.mge.gadgeothek.R;
import ch.hsr.mge.gadgeothek.domain.Loan;
import ch.hsr.mge.gadgeothek.domain.Reservation;
import ch.hsr.mge.gadgeothek.service.Callback;
import ch.hsr.mge.gadgeothek.service.LibraryService;

import static ch.hsr.mge.gadgeothek.util.UiTools.createTextView;
import static ch.hsr.mge.gadgeothek.util.UiTools.dpToPx;

public class StatusActivity extends BaseActivity {

    private static final int TEXT_SMALL = 16;

    private static final Callback<Void> NO_OP_CALLBACK = new Callback<Void>() {
        @Override
        public void onCompletion(Void input) {

        }

        @Override
        public void onError(String message) {

        }
    };

    // Cache found views for performance
    private TableLayout loanTableCache;
    private TableLayout reservationTableCache;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        checkLoggedIn();
        refreshStatus(NO_OP_CALLBACK);

        Button logoutButton = findTypedViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryService.logout(new Callback<Boolean>() {
                    @Override
                    public void onCompletion(Boolean input) {
                        if (input != null && input) {
                            checkLoggedIn();
                        } else {
                            showErrorDialog("Logout failed", "An unknown error occurred");
                        }
                    }

                    @Override
                    public void onError(String message) {
                        showErrorDialog("Logout failed", message);
                    }
                });
            }
        });

        Button reservationButton = findTypedViewById(R.id.reservationButton);
        reservationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CONTEXT, MakeReservationActivity.class));
            }
        });

        Button refreshButton = findTypedViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshStatus(new Callback<Void>() {
                    @Override
                    public void onCompletion(Void input) {
                        Toast.makeText(CONTEXT, "Status refreshed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String message) {

                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkLoggedIn();
        refreshStatus(NO_OP_CALLBACK);
    }

    private void refreshStatus(final Callback<Void> callback) {
        Callback<Class<?>> joiningCallback = new Callback<Class<?>>() {
            private boolean loanTableRefreshed = false;
            private boolean reservationTableRefreshed = false;

            @Override
            public void onCompletion(Class<?> input) {
                if (input == Loan.class) {
                    loanTableRefreshed = true;
                } else if (input == Reservation.class) {
                    reservationTableRefreshed = true;
                }

                if (loanTableRefreshed && reservationTableRefreshed) {
                    callback.onCompletion(null);
                }
            }

            @Override
            public void onError(String message) {
                // ignore errors
            }
        };

        refreshLoanTable(joiningCallback);
        refreshReservationTable(joiningCallback);
    }

    /**
     * Refreshes the loan table by removing all children and then re-populating the table
     * with the header-row and a newly created row for each element.
     *
     * If there are no loans, an information message is added instead.
     */
    private void refreshLoanTable(final Callback<Class<?>> callback) {
        final TableLayout loanTable = getLoanTable();

        LibraryService.getLoansForCustomer(new Callback<List<Loan>>() {
            @Override
            public void onCompletion(List<Loan> input) {
                TableRow header = (TableRow) loanTable.getChildAt(0);
                loanTable.removeAllViews();
                loanTable.addView(header);

                if (input.isEmpty()) {
                    TableRow row = new TableRow(CONTEXT);
                    row.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    loanTable.addView(row);

                    TextView text = createTextView(CONTEXT, "You currently have no gadgets loaned.", TEXT_SMALL);
                    row.addView(text);

                    TableRow.LayoutParams layoutParams = (TableRow.LayoutParams) text.getLayoutParams();
                    layoutParams.span = 3;
                    text.setLayoutParams(layoutParams);
                    callback.onCompletion(Loan.class);
                    return;
                }

                for (Loan loan : input) {
                    loanTable.addView(createTableRow(
                        loan.getGadget().getName(),
                        formatDate(loan.getPickupDate()),
                        formatDate(loan.overDueDate()),
                        null
                    ));
                }

                callback.onCompletion(Loan.class);
            }

            @Override
            public void onError(String message) {
                showErrorDialog("Server error", message);
                callback.onError(message);
            }
        });
    }

    private TableLayout getLoanTable() {
        if (loanTableCache == null) {
            loanTableCache = findTypedViewById(R.id.loanTable);
        }
        return loanTableCache;
    }

    /**
     * Refreshes the reservation table by removing all children and then re-populating the table
     * with the header-row and a newly created row for each element.
     *
     * If there are no reservations, an information message is added instead.
     */
    private void refreshReservationTable(final Callback<Class<?>> callback) {
        final TableLayout reservationTable = getReservationTable();

        LibraryService.getReservationsForCustomer(new Callback<List<Reservation>>() {
            @Override
            public void onCompletion(List<Reservation> input) {
                TableRow header = (TableRow) reservationTable.getChildAt(0);
                reservationTable.removeAllViews();
                reservationTable.addView(header);

                if (input.isEmpty()) {
                    TableRow row = new TableRow(CONTEXT);
                    row.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    reservationTable.addView(row);

                    TextView text = createTextView(CONTEXT, "You currently have no reservations.", TEXT_SMALL);
                    row.addView(text);

                    TableRow.LayoutParams layoutParams = (TableRow.LayoutParams) text.getLayoutParams();
                    layoutParams.span = 3;
                    text.setLayoutParams(layoutParams);
                    callback.onCompletion(Reservation.class);
                    return;
                }

                for (Reservation reservation : input) {
                    reservationTable.addView(createTableRow(
                        reservation.getGadget().getName(),
                        reservation.getWatingPosition() + "",
                        reservation.isReady() ? "Available" : "Not Available",
                        reservation
                    ));
                }

                callback.onCompletion(Reservation.class);
            }

            @Override
            public void onError(String message) {
                showErrorDialog("Server error", message);
                callback.onError(message);
            }
        });
    }

    private TableRow createTableRow(String col1Text, String col2Text, String col3Text, Reservation reservation) {
        TableRow row = new TableRow(CONTEXT);
        row.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        row.addView(createTextView(CONTEXT, col1Text, TEXT_SMALL));
        row.addView(createTextView(CONTEXT, col2Text, TEXT_SMALL));
        row.addView(createTextView(CONTEXT,col3Text, TEXT_SMALL));
        row.setPadding(dpToPx(5, CONTEXT), 0, dpToPx(5, CONTEXT), 0);
        row.setGravity(Gravity.CENTER_VERTICAL);

        Button deleteButtonTemplate = findTypedViewById(R.id.deleteButton);
        Button deleteButton = new Button(CONTEXT);
        deleteButton.setOnClickListener(new ReservationDeletionListener(reservation));
        deleteButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        deleteButton.setCompoundDrawables(deleteButtonTemplate.getCompoundDrawables()[0], null, null, null);
        deleteButton.setLayoutParams(deleteButtonTemplate.getLayoutParams());
        deleteButton.setPadding(deleteButtonTemplate.getPaddingLeft(), deleteButtonTemplate.getPaddingTop(), deleteButtonTemplate.getPaddingRight(), deleteButtonTemplate.getPaddingBottom());

        if (reservation == null) {
            deleteButton.setVisibility(View.INVISIBLE);
        }

        row.addView(deleteButton);
        return row;
    }

    private TableLayout getReservationTable() {
        if (reservationTableCache == null) {
            reservationTableCache = findTypedViewById(R.id.reservationTable);
        }
        return reservationTableCache;
    }

    private String formatDate(Date date) {
        return DateFormat.getDateFormat(CONTEXT).format(date);
    }

    private class ReservationDeletionListener implements View.OnClickListener {

        private final Reservation reservation;

        private ReservationDeletionListener(Reservation reservation) {
            this.reservation = reservation;
        }

        @Override
        public void onClick(View v) {
            AlertDialog dialog = new AlertDialog.Builder(CONTEXT)
                    .setTitle("Delete reservation")
                    .setMessage("Do you really want to delete your reservation for " + reservation.getGadget().getName() + "?")
                    .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, int which) {
                            LibraryService.deleteReservation(reservation, new Callback<Boolean>() {
                                @Override
                                public void onCompletion(Boolean input) {
                                    dialog.dismiss();
                                    if (input != null && input) {
                                        AlertDialog successDialog = new AlertDialog.Builder(CONTEXT)
                                                .setTitle("Reservation deleted")
                                                .setCancelable(false)
                                                .setMessage("Your reservation for " + reservation.getGadget().getName() + " has been deleted.")
                                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        refreshStatus(NO_OP_CALLBACK);
                                                    }
                                                })
                                                .create();
                                        successDialog.show();
                                    } else {
                                        showErrorDialog("Deletion failed", "An unknown error occured");
                                    }
                                }

                                @Override
                                public void onError(String message) {
                                    dialog.dismiss();
                                    showErrorDialog("Deletion failed", message);
                                }
                            });
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }
    }
}
