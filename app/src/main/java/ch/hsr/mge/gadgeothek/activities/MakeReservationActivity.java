package ch.hsr.mge.gadgeothek.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ch.hsr.mge.gadgeothek.R;
import ch.hsr.mge.gadgeothek.domain.Gadget;
import ch.hsr.mge.gadgeothek.service.Callback;
import ch.hsr.mge.gadgeothek.service.LibraryService;

public class MakeReservationActivity extends BaseActivity {

    private Gadget selectedGadget;

    private Button reservationButtonCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_reservation);

        loadItemSpinner();

        getReservationButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedGadget == null) {
                    showErrorDialog("Reservation failed", "Please select a gadget");
                    return;
                }

                LibraryService.reserveGadget(selectedGadget, new Callback<Boolean>() {
                    @Override
                    public void onCompletion(Boolean input) {
                        if (input == null || !input) {
                            showErrorDialog("Reservation failed", "An unknown error occurred");
                            return;
                        }

                        new AlertDialog.Builder(CONTEXT)
                            .setTitle("Reservation successful")
                            .setCancelable(false)
                            .setMessage("Your reservation for " + selectedGadget.getName() + " was successful.")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    }

                    @Override
                    public void onError(String message) {
                        showErrorDialog("Reservation failed", message);
                    }
                });
            }
        });
    }

    private void loadItemSpinner(){
        LibraryService.getGadgets(new Callback<List<Gadget>>() {
            @Override
            public void onCompletion(List<Gadget> input) {
                final Map<String, Gadget> gadgets = new LinkedHashMap<>();
                for (Gadget gadget : input) {
                    gadgets.put(gadget.getName(), gadget);
                }

                final Spinner spinner = findTypedViewById(R.id.spinner);

                final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
                        MakeReservationActivity.this,
                        R.layout.support_simple_spinner_dropdown_item,
                        new ArrayList<>(gadgets.keySet()));

                spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinner.setAdapter(spinnerArrayAdapter);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItemText = (String) parent.getItemAtPosition(position);
                        selectedGadget = gadgets.get(selectedItemText);
                        getReservationButton().setClickable(true);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        selectedGadget = null;
                        getReservationButton().setClickable(false);
                    }
                });
            }

            @Override
            public void onError(String message) {
                showErrorDialog("Server error", message);
            }
        });
    }

    private Button getReservationButton() {
        if (reservationButtonCache == null) {
            reservationButtonCache = findTypedViewById(R.id.submitReservationButton);
        }
        return reservationButtonCache;
    }
}