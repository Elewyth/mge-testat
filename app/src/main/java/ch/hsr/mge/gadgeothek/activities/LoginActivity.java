package ch.hsr.mge.gadgeothek.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ch.hsr.mge.gadgeothek.R;
import ch.hsr.mge.gadgeothek.service.Callback;
import ch.hsr.mge.gadgeothek.service.LibraryService;

import static ch.hsr.mge.gadgeothek.util.UiTools.emptyAsNull;

public class LoginActivity extends BaseActivity {

    static final String KEY_EMAIL = "email";
    static final String KEY_SERVER_ADDRESS = "serverAddress";

    private static final String DEFAULT_EMAIL = "yuhu@hsr.ch";
    private static final String DEFAULT_PASSWORD = "yuhu";
    static final String DEFAULT_SERVER_ADDRESS = "http://mge3.dev.ifs.hsr.ch/public";

    static final String LOGIN_SETTINGS = "loginSettings";

    // Cache found views for performance
    private EditText emailFieldCache;
    private EditText serverFieldCache;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText emailField = getEmailField();
        final EditText serverField = getServerField();

        SharedPreferences loginSettings = getSharedPreferences(LOGIN_SETTINGS, MODE_PRIVATE);
        String savedEmail = loginSettings.getString(KEY_EMAIL, DEFAULT_EMAIL);
        String savedServerAddress = loginSettings.getString(KEY_SERVER_ADDRESS, DEFAULT_SERVER_ADDRESS);
        Log.d(TAG, "Reading login settings: {Email=" + savedEmail + ",Server=" + savedServerAddress + "}");

        emailField.setText(savedEmail);
        serverField.setText(savedServerAddress);

        Button loginButton = findTypedViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText passwordField = findTypedViewById(R.id.password);

                String email = getEmailField().getText().toString();
                String password = passwordField.getText().toString();
                String serverAddress = getServerField().getText().toString();

                saveLoginSettings();

                if (email.isEmpty()) {
                    email = DEFAULT_EMAIL;
                }

                if (password.isEmpty()) {
                    password = DEFAULT_PASSWORD;
                }

                if (serverAddress.isEmpty()) {
                    serverAddress = DEFAULT_SERVER_ADDRESS;
                }

                LibraryService.setServerAddress(serverAddress);

                Log.d(TAG, "Performing Login");
                LibraryService.login(email, password, new Callback<Boolean>() {
                    @Override
                    public void onCompletion(Boolean input) {
                        if (input != null && input) {
                            Log.d(TAG, "Login successful");
                            finishLogin();
                        } else {
                            showErrorDialog("Login failed", "An unknown error occurred");
                        }
                    }

                    @Override
                    public void onError(String message) {
                        showErrorDialog("Login failed", message);
                    }
                });
            }
        });

        Button signUpButton = findTypedViewById(R.id.registerButton);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CONTEXT, RegisterActivity.class));
            }
        });
    }

    private EditText getEmailField() {
        if (emailFieldCache == null) {
            emailFieldCache = findTypedViewById(R.id.email);
        }
        return emailFieldCache;
    }

    private EditText getServerField() {
        if (serverFieldCache == null) {
            serverFieldCache = findTypedViewById(R.id.server);
        }
        return serverFieldCache;
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveLoginSettings();
    }

    private void saveLoginSettings() {
        SharedPreferences loginSettings = getSharedPreferences(LOGIN_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = loginSettings.edit();
        editor.putString(KEY_EMAIL, emptyAsNull(getEmailField().getText().toString()));
        editor.putString(KEY_SERVER_ADDRESS, emptyAsNull(getServerField().getText().toString()));
        editor.apply();
        Log.d(TAG, "Login settings saved.");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (LibraryService.isLoggedIn()) {
            finishLogin();
        }
    }

    private void finishLogin() {
        startActivity(new Intent(CONTEXT, StatusActivity.class));
        finish();
    }
}
