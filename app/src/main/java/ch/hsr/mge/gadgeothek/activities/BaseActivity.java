package ch.hsr.mge.gadgeothek.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import ch.hsr.mge.gadgeothek.service.LibraryService;

public abstract class BaseActivity extends AppCompatActivity {

    protected final String TAG = getClass().getSimpleName();
    protected final Context CONTEXT = this;

    protected <T extends View> T findTypedViewById(int id) {
        //noinspection unchecked
        return (T) findViewById(id);
    }

    protected void showErrorDialog(String title, String message) {
        Log.e(TAG, title + ": " + message);
        new AlertDialog.Builder(CONTEXT)
                .setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    protected void checkLoggedIn() {
        if (!LibraryService.isLoggedIn()) {
            startActivity(new Intent(CONTEXT, LoginActivity.class));
            finish();
        }
    }
}
