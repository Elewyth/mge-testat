package ch.hsr.mge.gadgeothek.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.TextView;

public final class UiTools {

    private UiTools() {
        // Tools-Class
    }

    public static TextView createTextView(Context context, String text) {
        TextView result = new TextView(context);
        result.setText(text);
        return result;
    }

    public static TextView createTextView(Context context, String text, int fontSize) {
        TextView result = new TextView(context);
        result.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
        result.setText(text);
        return result;
    }

    public static String emptyAsNull(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        }
        return s;
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
