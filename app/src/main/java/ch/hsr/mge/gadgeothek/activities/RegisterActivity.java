package ch.hsr.mge.gadgeothek.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ch.hsr.mge.gadgeothek.R;
import ch.hsr.mge.gadgeothek.service.Callback;
import ch.hsr.mge.gadgeothek.service.LibraryService;

import static ch.hsr.mge.gadgeothek.activities.LoginActivity.DEFAULT_SERVER_ADDRESS;
import static ch.hsr.mge.gadgeothek.activities.LoginActivity.KEY_SERVER_ADDRESS;
import static ch.hsr.mge.gadgeothek.activities.LoginActivity.LOGIN_SETTINGS;

public class RegisterActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        SharedPreferences loginSettings = getSharedPreferences(LOGIN_SETTINGS, MODE_PRIVATE);
        final String savedServerAddress = loginSettings.getString(KEY_SERVER_ADDRESS, DEFAULT_SERVER_ADDRESS);

        EditText serverField = findTypedViewById(R.id.server);
        serverField.setText(savedServerAddress);

        Button registerButton = findTypedViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = getFieldText(R.id.email);
                if (mail.isEmpty()) {
                    showErrorDialog("Registration incomplete", "Please enter your email address");
                    return;
                }

                final String password = getFieldText(R.id.password);
                if (password.isEmpty()) {
                    showErrorDialog("Registration incomplete", "Please enter a password");
                    return;
                }

                String passwordConfirm = getFieldText(R.id.passwordConfirm);
                if (passwordConfirm.isEmpty()) {
                    showErrorDialog("Registration incomplete", "Please confirm your password");
                    return;
                }
                if (!passwordConfirm.equals(password)) {
                    showErrorDialog("Registration incomplete", "Entered passwords do not match");
                    return;
                }

                String name = getFieldText(R.id.name);
                if (name.isEmpty()) {
                    showErrorDialog("Registration incomplete", "Please enter your name");
                    return;
                }

                String studentNumber = getFieldText(R.id.studentNumber);
                if (studentNumber.isEmpty()) {
                    showErrorDialog("Registration incomplete", "Please enter your student number");
                    return;
                }

                String serverAddress = getFieldText(R.id.server);
                if (serverAddress.isEmpty()) {
                    showErrorDialog("Registration incomplete", "Please enter the server URL");
                    return;
                }

                LibraryService.setServerAddress(serverAddress);
                LibraryService.register(mail, password, name, studentNumber, new Callback<Boolean>() {
                    @Override
                    public void onCompletion(Boolean input) {
                        if (input == null || !input) {
                            showErrorDialog("Registration failed", "An unknown error occurred");
                            return;
                        }

                        new AlertDialog.Builder(CONTEXT)
                            .setTitle("Registration successful")
                            .setMessage("Your account has been created successfully")
                            .setCancelable(false)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(final DialogInterface dialog, int which) {
                                    LibraryService.login(mail, password, new Callback<Boolean>() {
                                        @Override
                                        public void onCompletion(Boolean input) {
                                            finish();
                                            dialog.dismiss();

                                            if (input != null && input) {
                                                Log.d(TAG, "Login successful");
                                            } else {
                                                showErrorDialog("Login failed", "An unknown error occurred");
                                            }
                                        }

                                        @Override
                                        public void onError(String message) {
                                            finish();
                                            dialog.dismiss();
                                            showErrorDialog("Login failed", message);
                                        }
                                    });
                                }
                            })
                            .show();
                    }

                    @Override
                    public void onError(String message) {
                        showErrorDialog("Registration failed", message);
                    }
                });
            }
        });
    }

    private String getFieldText(int id) {
        EditText field = findTypedViewById(id);
        return field.getText().toString();
    }
}
